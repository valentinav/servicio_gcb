﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ServicioGCB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetricsController : ControllerBase
    {
        private HttpClient httpClient = HttpClientFactory.Create();
        private const string url = "http://localhost:4449";
        
        [HttpGet]
        public async Task<string> Get()
        {
            return await httpClient.GetStringAsync(url + "/selectedModel");
        }

    }
}
