﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServicioGCB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewLogController : ControllerBase
    {
        private HttpClient httpClient = HttpClientFactory.Create();
        private const string url = "http://localhost:4449";

        [HttpGet]
        public async Task<string> Get()
        {
            return await httpClient.GetStringAsync(url + "/processcsv");
        }
    }
}
