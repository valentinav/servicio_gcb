import { Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
    varVista: any;

    recibirVista($event: any) {
        this.varVista = $event;
    }
}
