import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataModelsService {
    private API_URL = 'http://localhost:5000/api';

    constructor(private http: HttpClient) { }

    public listModels() {
        return this.http.get(this.API_URL +'/RegModels');
    }
    public MetricsModel() {
        return this.http.get(this.API_URL + '/Metrics');
    }
    public PredictionsFromModel() {
        return this.http.get(this.API_URL + '/Predictions');
    }
    public LogWithChanges() {
        return this.http.get(this.API_URL + '/NewLog');
    }
}
