"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Modelmetrics = void 0;
var Modelmetrics = /** @class */ (function () {
    function Modelmetrics(name, ADJR2, R2, RMSE) {
        this.modelName = name;
        this.adjustedR_squared = ADJR2;
        this.r_squared = R2;
        this.rmse = RMSE;
    }
    return Modelmetrics;
}());
exports.Modelmetrics = Modelmetrics;
//# sourceMappingURL=modelmetrics.js.map