export class Prediction {
    predDate: string;
    weekday: number;
    workingHours: number;
    prediction: number;

    constructor(pDate: string, day: number, hours: number, pred: number) {
        this.predDate = pDate;
        this.weekday = day;
        this.workingHours = hours;
        this.prediction = pred;
    }

}
