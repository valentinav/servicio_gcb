"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Prediction = void 0;
var Prediction = /** @class */ (function () {
    function Prediction(pDate, day, hours, pred) {
        this.predDate = pDate;
        this.weekday = day;
        this.workingHours = hours;
        this.prediction = pred;
    }
    return Prediction;
}());
exports.Prediction = Prediction;
//# sourceMappingURL=prediction.js.map