export class Modelmetrics {

    modelName: string;
    adjustedR_squared: number;
    r_squared: number;
    rmse: number;

    constructor(name: string, ADJR2: number, R2: number, RMSE: number) {
        this.modelName = name;
        this.adjustedR_squared = ADJR2;
        this.r_squared = R2;
        this.rmse = RMSE;
    }

}
