import { Component, OnInit } from '@angular/core';
import { DataModelsService } from '../../service/data-models.service';

@Component({
  selector: 'app-newdatalog',
  templateUrl: './newdatalog.component.html',
  styleUrls: ['./newdatalog.component.css']
})
export class NewdatalogComponent {
    executionLogList = new Array();
    showLoading: boolean = true;

    constructor(private service: DataModelsService) {
        this.getLog()
    }

    getLog() {
        this.service.LogWithChanges().subscribe(res => {
            for (var i in res) {
                this.executionLogList.push(res[i]);
            }
            this.showLoading = false;
        });
    }

}
