import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewdatalogComponent } from './newdatalog.component';

describe('NewdatalogComponent', () => {
  let component: NewdatalogComponent;
  let fixture: ComponentFixture<NewdatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewdatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewdatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
