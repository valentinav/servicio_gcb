import { Component, OnInit } from '@angular/core';
import { Modelmetrics } from '../../interface/modelmetrics';
import { DataModelsService } from '../../service/data-models.service';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})
export class ModelsComponent {

    modelsList = new Array();
    showLoading: boolean = true;

    constructor(private service: DataModelsService) {
        this.listarModelos();
    }

    listarModelos() {
        this.service.listModels().subscribe(res => {
            for (var key in res) {
                var objModel = new Modelmetrics(key, res[key].AdjustedR_Squared, res[key].R_Squared, res[key].RMSE);
                this.modelsList.push(objModel);
            }
            this.showLoading = false;
        });
    }

}
