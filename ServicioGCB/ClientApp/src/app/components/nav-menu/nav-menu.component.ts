import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {

    @Output() vista: EventEmitter<string> = new EventEmitter<string>();
    mensajeVista: string;
    constructor() {
        this.mensajeVista = null;
    }

    cambioVista(vistaSeleccionada: string) {
        this.mensajeVista = vistaSeleccionada;
        this.vista.emit(this.mensajeVista);
    }
}
