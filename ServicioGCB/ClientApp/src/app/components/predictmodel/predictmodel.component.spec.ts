import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictmodelComponent } from './predictmodel.component';

describe('PredictmodelComponent', () => {
  let component: PredictmodelComponent;
  let fixture: ComponentFixture<PredictmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictmodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
