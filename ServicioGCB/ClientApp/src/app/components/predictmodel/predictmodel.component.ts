import { Component, OnInit } from '@angular/core';
import { Prediction } from '../../interface/prediction';
import { DataModelsService } from '../../service/data-models.service';

@Component({
  selector: 'app-predictmodel',
  templateUrl: './predictmodel.component.html',
  styleUrls: ['./predictmodel.component.css']
})
export class PredictmodelComponent {

    showLoading: boolean = true;
    RegModelMetrics: any;
    predictionsList = new Array();
    averageList: number = 0;

    constructor(private service: DataModelsService) {
        this.getSelectedModelMetrics();
    }

    getSelectedModelMetrics() {
        this.service.MetricsModel().subscribe(res => {
            this.RegModelMetrics = res;
        });
        this.getPredictions();
    }

    getPredictions() {
        this.service.PredictionsFromModel().subscribe(res => {
            for (var i in res) {
                var objPred = new Prediction(i, res[i].day, res[i].hours_day, res[i].prediction);
                this.predictionsList.push(objPred);
            }
            this.showLoading = false;
        });
    }

    getAveragePredictions() {

        for (var elem in this.predictionsList) {
            this.averageList += this.predictionsList[elem].prediction;
        }
        this.averageList = this.averageList / this.predictionsList.length;
        return this.averageList;
    }

}
