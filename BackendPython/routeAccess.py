from flask import Flask
from applyModel import applyRegModel, processwithchanges
from selectedModel import getMetricsFromModel
from bussinesProcess import lazyPredictRegressors

# Create an instance of the Flask class that is the WSGI application.
# The first argument is the name of the application module or package,
# typically __name__ when using a single module.
app = Flask(__name__)

# Flask route decorators map / and /hello to the hello function.
# To add other resources, create functions that generate the page contents
# and add decorators to define the appropriate resource locators for them.

#Activity Realizar confecciones
trainpath1 = './data/activity1RealizarConfecciones/train_confecciones.csv'
testpath1 = './data/activity1RealizarConfecciones/test_confecciones.csv'
datapath1 = './data/activity1RealizarConfecciones/cambiorecu_confeccion.csv'

#Activity preparar envío
trainpath2 = './data/activity2PreparaEnvio/train_prepararenvio.csv'
testpath2 = './data/activity2PreparaEnvio/test_prepararenvio.csv'
datapath2 = './data/activity2PreparaEnvio/cambiorecu_prepararenvio.csv'

@app.route('/')
@app.route('/infoModels')
def infoModels():
    return lazyPredictRegressors(trainpath1)

@app.route('/selectedModel')
def selectedModel():
    return getMetricsFromModel()

@app.route('/applyModel')
def applyModel():
    return applyRegModel(testpath1)

@app.route('/processcsv')
def getprocesscsv():
    return processwithchanges(datapath1)

if __name__ == '__main__':
    # Run the app server on localhost:4449
    app.run('localhost', 4449)