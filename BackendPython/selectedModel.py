from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import StratifiedKFold
import pickle
import numpy as np

infoMetrics= {}

def adjusted_rsquared(r2, n, p):
    return 1 - (1 - r2) * ((n - 1) / (n - p - 1))

def defineModel(model, modelName, X, Y):
    global infoMetrics
    
    ytestAcumulado = np.array([])
    ypredAcumulado = np.array([])
    filename = 'selectedModel.sav'
    
    kf = StratifiedKFold(n_splits=3, shuffle=True, random_state=2)
    
    for i_train, i_test in kf.split(X, Y):
        #print("Entrenamiento: ", i_train, 'Prueba: ', i_test)
        xtrain, xtest = X[i_train], X[i_test]
        ytrain, ytest = Y[i_train], Y[i_test]

        regmodel = model()
        regmodel.fit(xtrain, ytrain) 
        y_pred = regmodel.predict(xtest)

        ytestAcumulado = np.concatenate((ytestAcumulado,ytest), axis=None)
        ypredAcumulado = np.concatenate((ypredAcumulado,y_pred),axis=None)

    #METRICAS
    r2 =r2_score(ytestAcumulado, ypredAcumulado)
    adjr2 = adjusted_rsquared(r2, len(ytestAcumulado), len(ytestAcumulado))
    rmse = mean_squared_error(ytestAcumulado, ypredAcumulado, squared=False)
    
    regmodelFinal = model()
    regmodelFinal.fit(X, Y) 
    pickle.dump(regmodelFinal, open(filename,'wb'))
    
    infoMetrics = {
        'name': modelName,
        'R_Squared': r2,
        'AdjustedR_Squared' : adjr2 , 
        'RMSE': rmse
    }

def getMetricsFromModel():
    return infoMetrics
