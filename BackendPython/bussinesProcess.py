import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from lazypredict.Supervised import REGRESSORS, LazyRegressor
from selectedModel import defineModel
from windowing import windowing

def lazyPredictRegressors(trainPath):

    data = pd.read_csv(trainPath, sep=';')
    data = data.set_index('date')

    #Windowing of the dataset is performed
    windowing (data, attributes_to_windowing=['request'], 
        horizon_attributes=['request'], windows_size=3, horizon_size=1)

    x = np.array(data.drop(['request+1'], 1))
    y = np.array(data['request+1'])

    xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2, random_state=42)

    #All regressors from LazyPredict library are executed
    reg = LazyRegressor(verbose=0, ignore_warnings=True, custom_metric=None, predictions=False)
    models, predictions = reg.fit(xtrain, xtest, ytrain, ytest)

    jsonModels = models.to_json(orient = 'index')

    selectedModelName = models.index[0]

    for name, model in REGRESSORS:
        if(selectedModelName == name):
            selectedModel =model
            break
    
    defineModel(selectedModel, selectedModelName, x, y)

    return jsonModels

