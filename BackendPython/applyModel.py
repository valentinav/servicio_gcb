import pickle
import numpy as np
import pandas as pd
from windowing import windowing

def applyRegModel(testPath):
    
    datatest = pd.read_csv(testPath, sep=';')
    datatest = datatest.set_index('date')

    windowing (datatest, attributes_to_windowing=['request'], 
        horizon_attributes=['request'], windows_size=3, horizon_size=1)
        
    x = np.array(datatest.drop(['request+1'], 1))

    filename = 'selectedModel.sav'
    loaded_model = pickle.load(open(filename, 'rb'))
    y_pred = loaded_model.predict(x)
    
    df_pred = pd.DataFrame({
        "day": datatest['day'],
        "hours_day": datatest['hours_day'],
        "prediction": y_pred
    })
    
    pred_filename = testPath.replace('test', 'prediction')  
    df_pred.to_csv(pred_filename)
    jsonPred = df_pred.to_json(orient = 'index')
    return jsonPred

def processwithchanges(dataPath):
    newdata = pd.read_csv(dataPath, sep=';')
    jsonchange = newdata.to_json(orient = 'index')
    return jsonchange