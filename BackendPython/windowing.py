
def windowing (df, attributes_to_windowing, horizon_attributes, windows_size, horizon_size):
        # attributes_to_windowing is the columns list which windowing is applied
        # windows_size
        # horizon_attributes is the columns list to predict
        # horizon_size is the future time that the data will be projected

        for column in attributes_to_windowing:
                for v1 in range(0, windows_size):
                        df[column + "-" + str(windows_size - v1 - 1)] = df[column].shift(-v1)

        for column in horizon_attributes:
                for f in range(horizon_size, horizon_size+1):
                        df[column + "+" + str(f)] = df[column].shift(-(f + windows_size - 1))

        for column in attributes_to_windowing:
                del(df[column])
        
        #Remove empty data from windowing
        df.drop(df.tail(windows_size).index,inplace = True)